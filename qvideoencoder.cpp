#include "qvideoencoder.h"
#include <QDebug>

#if 0
// 압축되지 않은 현재 데이터를 담을 프레임 동적 할당
    frame = av_frame_alloc();
    if (!frame) {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }
    // 앞에서 설정한 값과 동일하게 설정
    frame->format = c->pix_fmt;
    frame->width  = c->width;
    frame->height = c->height;

    // 비디오 데이터 32장을 담을 수 있도록 버퍼 할당?(정확히 모르겠음)
    ret = av_frame_get_buffer(frame, 32);

#endif


uint8_t endcode[] = { 0, 0, 1, 0xb7 };

QVideoEncoder::QVideoEncoder(QObject *parent) : QObject(parent)
{

}
int QVideoEncoder::init()
{
    int retValue;
    //m_codec = avcodec_find_encoder_by_name("libx264");//AV_CODEC_ID_MPEG2VIDEO
    m_codec = avcodec_find_encoder(AV_CODEC_ID_MPEG2VIDEO);
    m_ctx_codec = avcodec_alloc_context3(m_codec);
    if (!m_ctx_codec) {
        qDebug() << "enc  encoding" << __LINE__;
        return -4;
    }
    m_packet = av_packet_alloc();
    if (!m_packet)
    {
        qDebug() << "encoding" << __LINE__;
        return -1;
    }


    /* put sample parameters */
    m_ctx_codec->bit_rate = 400000;// 비트레이트 설정 (압축률과 관련있음)
    /* resolution must be a multiple of two */
    m_ctx_codec->width = 352;
    m_ctx_codec->height = 288;
    /* frames per second */
    m_ctx_codec->time_base = (AVRational){1, 25};
    m_ctx_codec->framerate = (AVRational){25, 1};

    /* emit one intra frame every ten frames
    * check frame pict_type before passing frame
    * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
    * then gop_size is ignored and the output of encoder
    * will always be I frame irrespective to gop_size
    */
    m_ctx_codec->gop_size = 10;//gop 사이즈 설정. p 프레임(앞 영상과의 diff로 생성되는 영상)의 길이 설정
    m_ctx_codec->max_b_frames = 1;//b frame(뒤 영상과 diff로 생성되는 영상)의 길이 설정
    m_ctx_codec->pix_fmt = AV_PIX_FMT_YUV420P;

    if(m_codec->id == AV_CODEC_ID_H264)
    {
        qDebug() << "AV_CODEC_ID_H264";
        av_opt_set(m_ctx_codec->priv_data, "preset", "slow", 0);
        av_opt_set(m_ctx_codec->priv_data, "profile", "baseline", 0);
    }
    retValue = avcodec_open2(m_ctx_codec, m_codec, NULL);
    if (retValue < 0)
    {
        qDebug() << "enc avcodec_open2 error" << retValue;
        return -3;
    }

    f = fopen("C:\\Go\\d.mp4", "wb");
    if (!f)
    {
        qDebug() << "File Open Error";
    }

    m_frame = av_frame_alloc();
    if (!m_frame) {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }
    m_frame->format = m_ctx_codec->pix_fmt;
    m_frame->width  = m_ctx_codec->width;
    m_frame->height = m_ctx_codec->height;

    retValue = av_frame_get_buffer(m_frame, 100);
    if (retValue < 0) {
        fprintf(stderr, "Could not allocate the video frame data\n");
        exit(1);
    }
    int i, ret, x, y;
    /* encode 1 second of video */
    for ( i = 0; i < 25; i++) {
        fflush(stdout);

        /* make sure the frame data is writable */
        ret = av_frame_make_writable(m_frame);
        if (ret < 0)
            exit(1);

        /* prepare a dummy image */
        /* Y */
        for (y = 0; y < m_ctx_codec->height; y++) {
            for (x = 0; x < m_ctx_codec->width; x++) {
                m_frame->data[0][y * m_frame->linesize[0] + x] = x + y + i * 3;
            }
        }

        /* Cb and Cr */
        for (y = 0; y < m_ctx_codec->height/2; y++) {
            for (x = 0; x < m_ctx_codec->width/2; x++) {
                m_frame->data[1][y * m_frame->linesize[1] + x] = 128 + y + i * 2;
                m_frame->data[2][y * m_frame->linesize[2] + x] = 64 + x + i * 5;
            }
        }

        m_frame->pts = i;

        /* encode the image */
        test(m_frame);
        //encode(c, frame, pkt, f);
    }
    test(NULL);
    fwrite(endcode, 1, sizeof(endcode), f);
    fclose(f);

    avcodec_free_context(&m_ctx_codec);
    av_frame_free(&m_frame);
    av_packet_free(&m_packet);
    qDebug() << "Done";

    return 0;
}
int QVideoEncoder::init(AVCodecContext * ctx_codec, AVRational time_base)
{
    int retValue;

    m_codec = avcodec_find_encoder(AV_CODEC_ID_MPEG2VIDEO);
    //m_codec = avcodec_find_encoder_by_name("libx264");

    m_ctx_codec = avcodec_alloc_context3(m_codec);
    if (!m_ctx_codec) {
        qDebug() << "enc  encoding" << __LINE__;
        return -4;
    }
#if 1
    retValue = avcodec_get_context_defaults3(m_ctx_codec,m_codec);
    if(retValue)
    {
        qDebug() << __FUNCTION__ << retValue;
    }
#endif
#if 1

    /* put sample parameters */
    m_ctx_codec->bit_rate = ctx_codec->bit_rate;// 비트레이트 설정 (압축률과 관련있음)
    /* resolution must be a multiple of two */
    m_ctx_codec->width = ctx_codec->width;
    m_ctx_codec->height = ctx_codec->height;
    /* frames per second */
    m_ctx_codec->time_base = time_base; //(AVRational){1001, 30000};// fps에 맞게 설정
    m_ctx_codec->framerate = (AVRational){time_base.den, time_base.num};


    /* emit one intra frame every ten frames
    * check frame pict_type before passing frame
    * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
    * then gop_size is ignored and the output of encoder
    * will always be I frame irrespective to gop_size
    */
    m_ctx_codec->gop_size = 10;//gop 사이즈 설정. p 프레임(앞 영상과의 diff로 생성되는 영상)의 길이 설정
    m_ctx_codec->max_b_frames = 1;//b frame(뒤 영상과 diff로 생성되는 영상)의 길이 설정
    m_ctx_codec->pix_fmt = AV_PIX_FMT_YUV420P;

    if(m_codec->id == AV_CODEC_ID_H264)
    {
        qDebug() << "AV_CODEC_ID_H264";
        av_opt_set(m_ctx_codec->priv_data, "preset", "slow", 0);
        av_opt_set(m_ctx_codec->priv_data, "profile", "baseline", 0);
    }
#endif
    retValue = avcodec_open2(m_ctx_codec, m_codec, NULL);
    if (retValue < 0)
    {
        qDebug() << "enc avcodec_open2 error" << retValue;
        return -3;
    }

    f = fopen("C:\\Go\\d.mp4", "wb");
    if (!f)
    {
        qDebug() << "File Open Error";
    }
    m_packet = av_packet_alloc();
    if (!m_packet)
    {
        qDebug() << "encoding" << __LINE__;
        return -1;
    }


    return 0;
}
int QVideoEncoder::deinit()
{
    fwrite(endcode, 1, sizeof(endcode), f);
    fflush(f);
    fclose(f);
    return 0;
}

void QVideoEncoder::test(AVFrame *iframe)
{
    int ret;

    AVFrame * frame = iframe;

    if (!avcodec_is_open(m_ctx_codec))
    {
        qDebug() << "ERROE" << __LINE__;
    }
    if (!av_codec_is_encoder(m_ctx_codec->codec))
    {
        qDebug() << "ERROE" <<__LINE__;
    }

    ret = avcodec_send_frame(m_ctx_codec, frame);
    if (ret < 0) {
        qDebug() << "Error sending a frame for encoding" << ret;
        return;
    }

    while (ret >= 0) {
        // 인코딩된 결과를 받아옴
        ret = avcodec_receive_packet(m_ctx_codec, m_packet);
        // mp2에서는 필요없는 에러 체크이지만, h264에서는 필요함
        // 해당 에러는 프로세스 종료가 아니라, 한번 더 처리해야 함을 나타냄
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        {
            return;
        }
        else if (ret < 0) {
            qDebug() << "Error during encoding";
            return;
        }
        //qDebug() << "Write Packet " <<m_packet->duration << m_packet->pos << m_packet->pts <<  m_packet->size;

        // 인코딩 된 데이터는 pkt->data에 들어있음. 인코딩 사이즈는 pkt->size 임
        fwrite(m_packet->data, 1, m_packet->size, f);
        fflush(f);
        //pkt의 메모리를 해제함
        av_packet_unref(m_packet);

    }
}
