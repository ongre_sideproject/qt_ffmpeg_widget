#include "qvideodecoder.h"
#include <QDebug>
#include <QThread>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

QImage snapshot(AVCodecContext* dec_ctx, AVFrame* frame, int index)
{
    SwsContext* img_convert_ctx;

    img_convert_ctx = sws_getContext(dec_ctx->width,
                                     dec_ctx->height,
                                     dec_ctx->pix_fmt,
                                     dec_ctx->width,
                                     dec_ctx->height,
                                     AV_PIX_FMT_RGB24,
                                     SWS_BICUBIC, NULL, NULL, NULL);

    AVFrame* frameRGB = av_frame_alloc();
    avpicture_alloc((AVPicture*)frameRGB,
                    AV_PIX_FMT_RGB24,
                    dec_ctx->width,
                    dec_ctx->height);

    sws_scale(img_convert_ctx,
              frame->data,
              frame->linesize, 0,
              dec_ctx->height,
              frameRGB->data,
              frameRGB->linesize);

    QImage image(frameRGB->data[0],
                 dec_ctx->width,
                 dec_ctx->height,
                 frameRGB->linesize[0],
                 QImage::Format_RGB888);


    QString filename = QString("capure_%1.png").arg(index);
    return image;
    //image.save(filename);
}

QVideoDecoder::QVideoDecoder(QObject *parent) : QObject(parent)
{
    m_ctx_format = NULL;
    m_parameters = NULL;
    m_ctx_codec = NULL;
    m_codec = NULL;


    m_thread = new QThread;
    this->moveToThread(m_thread);
    m_thread->start();

}
QVideoDecoder::~QVideoDecoder()
{

}
void QVideoDecoder::onFileOpen(QString path)
{
    int ret =  _fileOpen(path);
    emit resultVideoFileOpen(ret);
}

void QVideoDecoder::onDecodeStart()
{
    test();
}

void QVideoDecoder::onStart(QString path)
{
    int ret =  _fileOpen(path);
    emit resultVideoFileOpen(ret);
    test();
    _deinit();
}
void QVideoDecoder::onFileClose()
{
    _deinit();
}

void QVideoDecoder::_deinit()
{
    if(m_ctx_codec)
    {
        avcodec_close(m_ctx_codec);
    }
    if(m_ctx_format)
    {
        avformat_close_input(&m_ctx_format);
    }

    m_ctx_format = NULL;
    m_parameters = NULL;
    m_ctx_codec = NULL;
    m_codec     = NULL;
}

int QVideoDecoder::_fileOpen(QString path)
{
    qDebug() << __FUNCTION__;
    string filename = "c:\\Go\\a.mp4";
    int retValue;

    if(m_ctx_format != NULL)
    {
        qDebug() << "m_ctx_format not null";
        return -1;
    }

    // 파일(비디오 소스) 오픈
    retValue = avformat_open_input(&m_ctx_format, filename.c_str(), NULL, NULL);
    if (retValue != 0)
    {
        qDebug() << "avformat_open_input error" << retValue;
        _deinit();
        return -1;
    }
    if (avformat_find_stream_info(m_ctx_format, NULL) < 0)
    {
        qDebug() << "avformat_find_stream_info error" << retValue;
        _deinit();
        return -1;
    }

    // 비디오 스트림 찾기
    m_video_stream_idx = -1;
    for (unsigned int i = 0; i < m_ctx_format->nb_streams; i++)
    {
        if (m_ctx_format->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            m_video_stream_idx = i;
            break;
        }
    }
    if (m_video_stream_idx == -1)
    {
        qDebug() << "Could not find video stream";
        _deinit();
        return -2;
    }


    // 비디오 코덱 오픈
    m_parameters = m_ctx_format->streams[m_video_stream_idx]->codecpar;
    m_codec = avcodec_find_decoder(m_parameters->codec_id);
    if (m_codec == NULL)
    {
        qDebug() << "avcodec_find_decoder error";
        _deinit();
        return -3;
    }
    m_ctx_codec = avcodec_alloc_context3(m_codec);
    if(!m_ctx_codec)
    {
        qDebug() << "avcodec_alloc_context3 error";
        _deinit();
        return -3;
    }
    retValue = avcodec_parameters_to_context(m_ctx_codec, m_parameters);
    if (retValue != 0)
    {
        qDebug() << "avcodec_parameters_to_context error" << retValue;
        _deinit();
        return -3;
    }
    retValue = avcodec_open2(m_ctx_codec, m_codec, NULL);
    if (retValue < 0)
    {
        qDebug() << "avcodec_open2 error" << retValue;
        _deinit();
        return -3;
    }

    return 0;
}
#define ENCODING

int QVideoDecoder::test()
{


    AVFrame * frame = av_frame_alloc();
    if (!frame)
    {
        return -4;
    }

    AVRational time_base;
    time_base.num = m_ctx_format->streams[m_video_stream_idx]->r_frame_rate.den;
    time_base.den = m_ctx_format->streams[m_video_stream_idx]->r_frame_rate.num;
#ifdef ENCODING
    m_encoder.init(m_ctx_codec,time_base);
#else
    m_encoder.init();
#endif

    AVPacket * packet;
    packet = av_packet_alloc();

    qDebug() << "**********INFO************";
    qDebug() << m_ctx_codec->bit_rate;
    qDebug() << m_ctx_codec->width;
    qDebug() << m_ctx_codec->height;
    qDebug() << m_ctx_codec->gop_size;
    qDebug() << m_ctx_codec->max_b_frames;
    qDebug() << m_ctx_format->streams[m_video_stream_idx]->r_frame_rate.num << m_ctx_format->streams[m_video_stream_idx]->r_frame_rate.den;
    qDebug() << "**********INFO************";

    // 파일(비디오 소스) 반복해서 끝까지 읽기
    while (av_read_frame(m_ctx_format, packet) >= 0)
    {
        fflush(stdout);
        // 비디오 스트림만 처리
        if (packet->stream_index == m_video_stream_idx)
        {
            int ret = avcodec_send_packet(m_ctx_codec, packet) < 0;
            if (ret < 0) {
                qDebug() << "Error sending a packet for decoding";
                return -5;
            }


            while (ret >= 0)
            {
                ret = avcodec_receive_frame(m_ctx_codec, frame);
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
                {
                    break;
                } else if (ret < 0)
                {
                    qDebug() << "Error sending a packet for decoding";
                    return -5;
                }

                //frame->pts = i;

                //qDebug() << "Read Frame" << frame->pts << frame->pkt_size << frame->pkt_pos;
                //qDebug() << "Read Packet " <<packet->duration << packet->pos << packet->pts <<  packet->size;

                emit takeSnapShot(snapshot(m_ctx_codec,frame,m_index));
                m_index++;
#ifdef ENCODING
                m_encoder.test(frame);
#endif
                av_frame_unref(frame);
            }

        }

        av_packet_unref(packet);
    }

    qDebug() << "Done";
#ifdef ENCODING
    m_encoder.test(NULL);
    m_encoder.deinit();
#endif
    av_frame_free(&frame);
    av_packet_free(&packet);

    emit resultDecode(0);
    return 0;
}
