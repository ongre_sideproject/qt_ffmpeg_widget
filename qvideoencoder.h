#ifndef QVIDEOENCODER_H
#define QVIDEOENCODER_H

#include <QObject>
#include <QIODevice>
#include <QFile>
#include <QImage>
#include <QThread>

extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libavutil/time.h>
    #include <libavutil/pixfmt.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
    #include <libavformat/avio.h>
    #include <libswscale/swscale.h>
    #include <libavutil/opt.h>

}
using namespace std;

class QVideoEncoder : public QObject
{
    Q_OBJECT
public:
    explicit QVideoEncoder(QObject *parent = nullptr);

    int init();
    int init(AVCodecContext * ctx_codec,AVRational time_base);
    int deinit();
    void test(AVFrame * iframe);


private:
    AVFormatContext     * m_ctx_format;
    AVCodecParameters   m_parameters;
    AVCodecContext      * m_ctx_codec;
    AVCodec             * m_codec;
    AVPacket            * m_packet;
    AVFrame             * m_frame;

    FILE *f;

    QThread             * m_thread;

};

#endif // QVIDEOENCODER_H
