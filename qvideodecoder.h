#ifndef QVIDEODECODER_H
#define QVIDEODECODER_H

#include <QObject>
#include <QIODevice>
#include <QFile>
#include <QImage>
#include <QThread>
#include "qvideoencoder.h"

extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libavutil/time.h>
    #include <libavutil/pixfmt.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
    #include <libavutil/imgutils.h>
}
using namespace std;


class QVideoDecoder : public QObject
{
    Q_OBJECT
public:
    explicit QVideoDecoder(QObject *parent = nullptr);
    ~QVideoDecoder();

    int test();

signals:
    void resultVideoFileOpen(int result);
    void resultDecode(int result);
    void takeSnapShot(QImage image);


public slots:
    void onFileOpen(QString path);
    void onDecodeStart();
    void onStart(QString path);
    void onFileClose();


private:
    void _deinit();
    int _fileOpen(QString path);

    AVFormatContext     * m_ctx_format;
    AVCodecParameters   * m_parameters;
    AVCodecContext      * m_ctx_codec;
    AVCodec             * m_codec;
    int                   m_video_stream_idx;

    QThread             * m_thread;

    QVideoEncoder         m_encoder;

    int                   m_index;
};

#endif // QVIDEODECODER_H
