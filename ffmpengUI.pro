QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

DESTDIR = ../_build

OBJECTS_DIR = $$DESTDIR/$$TARGET/.obj
MOC_DIR = $$DESTDIR/$$TARGET/.moc
RCC_DIR = $$DESTDIR/$$TARGET/.qrc
UI_DIR = $$DESTDIR/$$TARGET/.ui

INCLUDEPATH += $$PWD/ffmpeg/include

LIBS += -L$$PWD/ffmpeg/lib -lavcodec -lavdevice -lavfilter -lavformat -lavutil -lpostproc -lswresample -lswscale


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    qvideodecoder.cpp \
    qvideoencoder.cpp

HEADERS += \
    mainwindow.h \
    qvideodecoder.h \
    qvideoencoder.h

FORMS += \
    mainwindow.ui
