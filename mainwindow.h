#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>

#include "qvideodecoder.h"



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void decodeFileOpen(QString path);
    void decodeDecodeStart();
    void decodeStart(QString path);


private slots:
    void on_btnTest_clicked();
    void on_btnTestEnc_clicked();
    void onResultVideoFileOpen(int result);

    void on_pushButton_clicked();

    void onTakeSnapShot(QImage image);
private:
    Ui::MainWindow *ui;
    void initUi();

    QVideoDecoder  *decoder;
};
#endif // MAINWINDOW_H
