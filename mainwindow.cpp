#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initUi();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initUi()
{
    decoder = new QVideoDecoder;

    connect(this, SIGNAL(decodeFileOpen(QString)),decoder,SLOT(onFileOpen(QString)));
    connect(this, SIGNAL(decodeDecodeStart()),decoder,SLOT(onDecodeStart()));
    connect(this, SIGNAL(decodeStart(QString)),decoder,SLOT(onStart(QString)));
    connect(decoder, SIGNAL(resultVideoFileOpen(int)),this,SLOT(onResultVideoFileOpen(int)));
    connect(decoder, SIGNAL(takeSnapShot(QImage)),this,SLOT(onTakeSnapShot(QImage)));

}

void MainWindow::onTakeSnapShot(QImage image)
{
    ui->label->setPixmap(QPixmap::fromImage(image));
}

void MainWindow::onResultVideoFileOpen(int result)
{
    qDebug() << __FUNCTION__ << result;
}

void MainWindow::on_btnTest_clicked()
{
    emit decodeStart("");
    //decoder->onFileOpen("");
}

void MainWindow::on_btnTestEnc_clicked()
{
    //decoder->onDecodeStart();
    emit decodeDecodeStart();
}

void MainWindow::on_pushButton_clicked()
{
    decoder->onFileClose();
}
